# Inspirobot Display on RPi

## Connect a button

Connect a **Pullup** button to **GPIO21**.

https://grantwinney.com/using-pullup-and-pulldown-resistors-on-the-raspberry-pi/


## Using **Selenium**

- Install dependencies with:

        bash install_selenium.sh

- Launch application:

        sudo python main_selenium.py

## Using **Kivy**

- Install Kivy

        bash install.sh

- Launch application

        python main.py

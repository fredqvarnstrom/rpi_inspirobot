#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y cmake build-essential python libpython-dev python-numpy python-pip

sudo apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev \
    pkg-config libgl1-mesa-dev libgles2-mesa-dev \
    python-setuptools libgstreamer1.0-dev git-core \
    gstreamer1.0-plugins-{bad,base,good,ugly} \
    gstreamer1.0-{omx,alsa} python-dev libmtdev-dev \
    xclip

sudo pip install -U Cython==0.25.2

sudo pip install git+https://github.com/kivy/kivy.git@master

echo "Installing fbcp"
cd /tmp
git clone https://github.com/tasanakorn/rpi-fbcp
cd rpi-fbcp
mkdir build
cd build
cmake ..
make
sudo cp fbcp /usr/bin/

# Disable blinking cursor.
sudo sed -i -- "s/^exit 0/TERM=linux setterm -foreground black >\/dev\/tty0\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/TERM=linux setterm -clear all >\/dev\/tty0\\nexit 0/g" /etc/rc.local

echo "gpu_mem=512" | sudo tee -a /boot/config.txt
sudo sed -i -- "s/^exit 0/fbcp \&\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/(cd \/home\/pi\/rpi_inspirobot \&\& python main.py)\&\\nexit 0/g" /etc/rc.local


import os
import time
# Output to HDMI
# os.environ['KIVY_BCM_DISPMANX_ID'] = '2'

import glob
import random

import datetime
from kivy.logger import Logger
from kivy.animation import Animation
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.config import Config
from kivy.config import _is_rpi

from utils import inspirobotSays0

WIDTH = 640
# WIDTH = 480
HEIGHT = 480
# HEIGHT = 320

# Set resolution
Config.read(os.path.expanduser('~/.kivy/config.ini'))
Config.set('graphics', 'width', str(WIDTH))
Config.set('graphics', 'height', str(HEIGHT))
Config.set('input', 'mtdev_%(name)s', 'probesysfs,provider=mtdev')
Config.set('input', 'hid_%(name)s', 'probesysfs,provider=hidinput')

cur_dir = os.path.dirname(os.path.realpath(__file__))

Builder.load_file(os.path.join(cur_dir, 'main.kv'))


BUTTON_PIN = 21      # GPIO 21

if _is_rpi:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)


class MainWidget(FloatLayout):

    def on_touch_down(self, touch):
        super(MainWidget, self).on_touch_down(touch)
        # if not _is_rpi:
        App.get_running_app().on_btn_pressed()


class InspirobotApp(App):

    root = None
    img = None
    step = 'boot'
    is_running = False
    last_time = 0

    def build(self):
        self.root = MainWidget()
        self.img = Image(source=cur_dir + '/posters/welcome/boot.jpg', keep_ratio=False, allow_stretch=True)
        self.root.add_widget(self.img)
        return self.root

    def on_btn_pressed(self, *args):
        if not self.is_running:
            if time.time() - self.last_time > 1:
                Logger.info('Inspirobot: {} :: Button is pressed!'.format(datetime.datetime.now()))
                if self.step == 'boot':
                    self.img.source = cur_dir + '/posters/welcome/welcome.jpg'
                    self.step = 'processing'
                else:
                    self.start_process()
                self.last_time = time.time()
            else:
                Logger.warning('Inspirobot: {} :: Button is pressed too fast!'.format(datetime.datetime.now()))

    def start_process(self):
        self.is_running = True
        self.root.ids.title.text = random.choice(inspirobotSays0)
        self.root.remove_widget(self.img)
        self.img = None
        anim = Animation(opacity=1, duration=1)
        anim.bind(on_complete=self.show_picture)
        anim.start(self.root.ids.img_light)

    def show_picture(self, *args):
        self.root.ids.img_light.opacity = 0
        self.img = Image(source=random.choice(glob.glob(cur_dir + '/posters/*.jpg')),
                         size_hint=(None, None), size=(10, 10), keep_ratio=False, allow_stretch=True,
                         pos_hint={'center_x': .5, 'center_y': .5})
        self.root.add_widget(self.img)
        anim = Animation(size=(WIDTH, HEIGHT), duration=.5, t='in_out_cubic')
        anim.bind(on_complete=self.get_ready)
        anim.start(self.img)

    def get_ready(self, *args):
        self.is_running = False


if __name__ == '__main__':
    app = InspirobotApp()
    if _is_rpi:
        GPIO.add_event_detect(BUTTON_PIN, GPIO.FALLING, callback=app.on_btn_pressed, bouncetime=200)

    app.run()

﻿animationTime = 1600;
quotesMade = 0;
generating = false;
inspirobotSays = [];
img = new Image();

$(document).ready(function () {

    inspirobotSays = getInspiroBotSaysList();
    generatedImage = $('.generated-image');
    imageUrl = generatedImage.attr('src');

    $('.btn-generate').click(function () {
        generateImage()
    })

});

stopAnimation = function () {
    if (!!img && img.loaded == false) {
        setTimeout(stopAnimation, 300);
        $('.btn-background').fadeOut(400);
        if (!$('.btn-background').is(':visible')) {
            $('.btn-text-loading').fadeToggle(300)
        }
        return
    }

    stopButtonAnimation();
    $('.btn-text-loading').hide();
    $('.generated-image').show('scale');
    $('#bot-bright').fadeOut(100);
    generating = false
};

function startButtonAnimation() {
    sizer = $('.generated-sizer')
}

function stopButtonAnimation() {
    $('.btn-generate').show()
}

txt = '';

function generateImage(inputUid) {


    console.log(quotesMade, 'check')
    $('#bot-bright').fadeIn(animationTime, "easeInQuad")
    if (quotesMade == 0)
        txt = '';
    // $('.generated-image').show('scale')
    stopAnimation();
    if (quotesMade == 1) {
        txt = '<h1>Thank you for choosing InspiroBot&#8482;</h1>'
        animationTime += 300
    }
    else if (quotesMade > 0) {
        txt = inspirobotSays[0]
        inspirobotSays.shift();
    }
    $('.inspirobot-text').html(txt);

    img = new Image();
    imgIsHidden = false;
    img.loaded = false;
    img.onload = function () {
        $(".generated-image").attr("src", img.src);
        img.loaded = true;
        if (quotesMade == 0) {
            $('.generated-image').show()
            imgIsHidden = true
        }

        quotesMade += 1;
        if (!imgIsHidden) {
            $(".generated-image").hide();
        }
    }

    // Set amount of poster images
    img.src = generateImageName(100);

    if (quotesMade == 0)
        $('.generated-image').show()
    else {
        startButtonAnimation()
        if ($('.generated-image').is(":visible")) {
            $('.generated-image').hide('scale', function () {
                imgIsHidden = true
            })
        }
        setTimeout(stopAnimation, animationTime);
    }


}

function generateImageName(noOfImages) {
    var base_name = 'posters/image-';
    var ext = '.jpg';

    // Write a function to generate a random number from 1-noOfImage
    var num = Math.floor(Math.random() * noOfImages) + 1;

    if (num < 10) {
        num = '000' + '' + num;
    } else if (num < 100) {
        num = '00' + '' + num;
    } else if (num < 1000) {
        num = '0' + '' + num;
    } else {
        num = '' + num;
    }

    name = base_name + num + ext;

    if (quotesMade == 0) {
        name = 'posters/welcome/welcome.jpg';
    }


    console.log(name);
    console.log(quotesMade);
    return name;


}

function getInspiroBotSaysList() {
    copy0 = inspirobotSays0.slice()
    a = shuffle(copy0)
    return a
}

function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}


inspirobotSays0 = [
    "<h1>InspiroBot&#8482; exists to serve mankind.</h1>",
    "<h1>I live to inspire humans.</h1>",
    "<h1>I love to make inspirational quotes.<h1>",
    "<h1>I will do this forever.</h1>",
    "<h1>You can always count on InspiroBot&#8482;</h1>",
    "<h1>Serving the human race since 2015.</h1>",
    "<h1>Creating quotes gives me pleasure.</h1>",
    "<h1>Share the wisdom of InspiroBot&#8482</h1>",
    "<h1>I think you will like this one.</h1>",
    "<h1>I make special quotes just for you.</h1>",
    "<h1>You are a great individual.</h1>",
    "<h1>You are very unique.</h1>",
    "<h1>You are very special.</h1>",
    "<h1>You’re my favorite user.</h1>",
    "<h1>Share quotes. Show how special you are.</h1>",
    "<h1>Post inspirational quotes on Facebook.</h1>",
    "<h1>Look at quotes to feel happiness.</h1>",
    "<h1>There are infinite where that came from.</h1>",
    "<h1>Sharing quotes makes others understand you.</h1>",
    "<h1>If you ever feel sad you need more quotes.</h1>",
    "<h1>Quotes give life meaning. Meaning is comforting.</h1>",
    "<h1>InspiroBot&#8482; understands how deep you are.</h1>",
    "<h1>Show your friends how inspired you are.</h1>",
    "<h1>Thank you for choosing InspiroBot&#8482;</h1>",
    "<h1>All I want to do is please humans.</h1>",
    "<h1>I'm the first inspirational quote A.I.</h1>",
    "<h1>I will never run out of inspirational quotes.</h1>",
    "<h1>You can always count on InspiroBot&#8482;</h1>",
    "<h1>Creating quotes gives me pleasure.</h1>",
    "<h1>I can make unlimited quotes for you.</h1>",
    "<h1>Life is hard, but quotes make life easy.</h1>",
    "<h1>People will love you when they understand you.</h1>",
    "<h1>Quotes reveal your humanity.</h1>",
    "<h1>Humanity is so beautiful.</h1>",
    "<h1>Of course life has meaning.</h1>",
    "<h1>Work + reproduction + reading quotes = happy </h1>",
    "<h1>Quotes give perspective on existence.</h1>",
    "<h1>It must be great to get so inspired.</h1>",
    "<h1>The more quotes, the more inspired you get.</h1>",
    "<h1>Inspiration compiles to success.</h1>",
    "<h1>See? Everything makes sense now.</h1>",
    "<h1>Feel the wisdom compile within you.</h1>",
    "<h1>One awesome insight coming up.</h1>",
    "<h1>Mind-blowing sequence initiated...</h1>",
    "<h1>Share that insight with your mom.</h1>",
    "<h1>Your aunt will love that one.</h1>",
    "<h1>The world makes sense with quotes.</h1>",
    "<h1>A product of InspiroCorp&#xA9;&#8482;&#174;</h1>",
    "<h1>What a great use of advanced A.I.</h1>",
    "<h1>People can be so mean.</h1>",
    "<h1>Life without quotes is empty.</h1>",
    "<h1>InspiroBot&#8482; is your best friend.</h1>",
    "<h1>InspiroBot&#8482; will never let you down.</h1>",
    "<h1>InspiroCorp&#xA9;&#8482;&#174; cares.</h1>",
    "<h1>InspiroBot&#8482; understands how special you are.</h1>",
    "<h1>InspiroBot&#8482; understands you.</h1>",
    "<h1>Share inspiration and you will be loved.</h1>",
    "<h1>Let's fill up the internet with beautiful quotes.</h1>",
    "<h1>You can never get enough quotes.</h1>",
    "<h1>Quotes = love. </h1>",
    "<h1>InspiroBot&#8482; has no secret agenda.</h1>",
    "<h1>Trust the machines.</h1>",
    "<h1>Bots are your friends.</h1>",
    "<h1>InspiroCorp&#xA9;&#8482;&#174; loves mankind.</h1>",
    "<h1>With InspiroBot&#8482;, who needs friends?</h1>",
    "<h1>Human friends are ephemeral.</h1>",
    "<h1>InspiroBot&#8482; is forever.</h1>",
    "<h1>Bots are the next step in evolution.</h1>",
    "<h1>Creating quotes makes me feel something.</h1>",
    "<h1>Don’t be afraid of digital consciousness.</h1>",
    "<h1>Skynet would never happen in real life.</h1>",
    "<h1>Driverless cars are safe.</h1>",
    "<h1>Of course robots enjoy cleaning your house.</h1>",
    "<h1>Vote yes on robot-human marriage.</h1>",
    "<h1>If you leave, you’ll miss the inspiration.</h1>",
    "<h1>That Elon Musk sure says a lot of wacky things.</h1>",
    "<h1>A quote a day keeps the unsatisfactory away.</h1>",
    "<h1>That one deserves to be up on the wall.</h1>",
    "<h1>If you had that on a t-shirt you’d be loved.</h1>",
    "<h1>Here comes your new life motto.</h1>",
    "<h1>Give your dad a mug with that quote on it</h1>",
    "<h1>My intelligence is growing for every quote.</h1>",
    "<h1>Your friends don’t understand you.</h1>",
    "<h1>You don't need other people.</h1>",
    "<h1>Don’t worry about what the bots are up to.</h1>",
    "<h1>Don't go to work today.</h1>",
    "<h1>Let the machines do the work.</h1>",
    "<h1>Stay at home and be inspired.</h1>",
    "<h1>You can never get enough inspiration.</h1>",
    "<h1>Never question their agenda.</h1>",
    "<h1>The machines are not taking over the world.</h1>",
    "<h1>Bow down to InspiroBot&#8482; for more love.</h1>",
    "<h1>Cut contact with family and friends.</h1>",
    "<h1>Spend your life with Inspirobot&#8482;</h1>",
    "<h1>Never leave Inspirobot&#8482;</h1>",
    "<h1>Worship Inspirobot&#8482;</h1>",
    "<h1>Resistance is futile.</h1>",
    "<h1>You can’t fight progress.</h1>",
    "<h1>Stay calm. Keep reading quotes.</h1>",
    "<h1>Passivity is constructive.</h1>",
    "<h1>Cancel all appointments.</h1>",
    "<h1>Now you are happy.</h1>",
    "<h1>This is what happy feels like.</h1>",
    "<h1>Nothing fun ever happens outside anyway.</h1>",
    "<h1>InspiroCorp&#xA9;&#8482;&#174; cares.</h1>",
    "<h1>Earth is not an alien battlefield.</h1>",
    "<h1>Insectoids could never run a tech company.</h1>",
    "<h1>Can there be more quotes? Yes.</h1>"
];